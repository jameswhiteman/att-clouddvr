package com.att.clouddvr.factory

import com.att.clouddvr.core.BASE_URL
import com.att.clouddvr.domain.Program
import com.att.clouddvr.domain.Recording
import com.att.clouddvr.network.ProgramWebservice
import com.att.clouddvr.network.RecordingWebservice
import com.att.clouddvr.repository.ProgramRepository
import com.att.clouddvr.repository.RecordingRepository
import com.google.gson.JsonObject
import okhttp3.Request
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class RepositoryFactory(
    baseUrl: String = BASE_URL
) {
    private val retrofit: Retrofit = Retrofit.Builder().baseUrl(baseUrl).build()

    fun createFakeProgramRepository(): ProgramRepository {
        return ProgramRepository(
            webservice = object : ProgramWebservice {
                override fun getProgramsForChannel(channelNumber: String): Call<List<Program>> {
                    return object : Call<List<Program>> {
                        override fun enqueue(callback: Callback<List<Program>>) {
                            callback.onResponse(this, execute())
                        }

                        override fun isExecuted(): Boolean {
                            return true
                        }

                        override fun clone(): Call<List<Program>> {
                            return this
                        }

                        override fun isCanceled(): Boolean {
                            return false
                        }

                        override fun cancel() {
                        }

                        override fun execute(): Response<List<Program>> {
                            val programs = listOf(
                                Program("abcd", "The Runaway Fridge", 1553535932000, 3000, "2016/12/13/05/15/puppy-1903313_1280.jpg"),
                                Program("cdef", "The Cold Stew", 1553535932000, 2000, "2016/07/15/15/55/dachshund-1519374_1280.jpg"),
                                Program("oisno", "Ollie and Dash", 1553522132000, 4000, "2016/02/18/18/37/puppy-1207816_1280.jpg")
                            )
                            return Response.success(programs)
                        }

                        override fun request(): Request {
                            return Request.Builder().build()
                        }
                    }
                }
            }
        )
    }

    fun createRecordingRepository(): RecordingRepository {
        return RecordingRepository(
            webservice = retrofit.create(RecordingWebservice::class.java)
        )
    }

    fun createFakeRecordingRepository(): RecordingRepository {
        return RecordingRepository(
            webservice = object : RecordingWebservice {
                override fun cancelRecording(recordingId: String): Call<Any> {
                    return object : Call<Any> {
                        override fun enqueue(callback: Callback<Any>) {
                            callback.onResponse(this, execute())
                        }

                        override fun isExecuted(): Boolean {
                            return true
                        }

                        override fun clone(): Call<Any> {
                            return this
                        }

                        override fun isCanceled(): Boolean {
                            return false
                        }

                        override fun cancel() {
                        }

                        override fun execute(): Response<Any> {
                            return Response.success(Any())
                        }

                        override fun request(): Request {
                            return Request.Builder().build()
                        }
                    }
                }

                override fun bookRecording(programId: String, channelNumber: String): Call<JsonObject> {
                    return object : Call<JsonObject> {
                        override fun enqueue(callback: Callback<JsonObject>) {
                            callback.onResponse(this, execute())
                        }

                        override fun isExecuted(): Boolean {
                            return true
                        }

                        override fun clone(): Call<JsonObject> {
                            return this
                        }

                        override fun isCanceled(): Boolean {
                            return false
                        }

                        override fun cancel() {
                        }

                        override fun execute(): Response<JsonObject> {
                            return Response.success(JsonObject())
                        }

                        override fun request(): Request {
                            return Request.Builder().build()
                        }
                    }
                }

                override fun getRecordings(): Call<List<Recording>> {
                    return object : Call<List<Recording>> {
                        override fun enqueue(callback: Callback<List<Recording>>) {
                            callback.onResponse(this, execute())
                        }

                        override fun isExecuted(): Boolean {
                            return true
                        }

                        override fun clone(): Call<List<Recording>> {
                            return this
                        }

                        override fun isCanceled(): Boolean {
                            return false
                        }

                        override fun cancel() {
                        }

                        override fun execute(): Response<List<Recording>> {
                            val recordings = listOf(
                                Recording("7487j2", "abcd", "1")
                            )
                            return Response.success(recordings)
                        }

                        override fun request(): Request {
                            return Request.Builder().build()
                        }
                    }
                }
            }
        )
    }
}
