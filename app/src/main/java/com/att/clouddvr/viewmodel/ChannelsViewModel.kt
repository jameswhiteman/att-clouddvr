package com.att.clouddvr.viewmodel

import android.view.View
import com.att.clouddvr.core.NavigationService
import com.att.clouddvr.viewstate.ChannelOverviewViewState
import com.att.clouddvr.viewstate.ChannelsViewState

class ChannelsViewModel(
    private val viewState: ChannelsViewState,
    private val navigationService: NavigationService<String>
) {
    // Static list of channels
    private val channels: Collection<String> = setOf(1.toString(), 2.toString(), 3.toString())

    init {
        update()
    }

    private fun update() {
        var items: Collection<ChannelOverviewViewState> = setOf()

        channels.forEach { channel ->
            val item = ChannelOverviewViewState(
                id = channel,
                title = channel,
                onClickListener = View.OnClickListener {
                    navigationService.navigateTo(channel)
                }
            )
            items = items.plus(element = item)
        }

        viewState.adapter.setItems(items)
    }
}