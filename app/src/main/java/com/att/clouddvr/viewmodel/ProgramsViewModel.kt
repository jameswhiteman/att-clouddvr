package com.att.clouddvr.viewmodel

import android.content.res.Resources
import android.view.View
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.att.clouddvr.R
import com.att.clouddvr.core.NavigationService
import com.att.clouddvr.domain.Program
import com.att.clouddvr.domain.Recording
import com.att.clouddvr.repository.ProgramRepository
import com.att.clouddvr.repository.RecordingRepository
import com.att.clouddvr.viewstate.ProgramOverviewViewState
import com.att.clouddvr.viewstate.ProgramsViewState
import java.text.DateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class ProgramsViewModel(
    lifecycleOwner: LifecycleOwner,
    programRepository: ProgramRepository,
    private val channel: String,
    private val baseUrl: String,
    private val viewState: ProgramsViewState,
    private val recordingRepository: RecordingRepository,
    private val resources: Resources,
    private val navigationService: NavigationService<Int>
) {
    private var loading: Boolean = false
    private var programs: Collection<Program> = setOf()
    private var recordings: Collection<Recording> = setOf()
    private var moment: Long = System.currentTimeMillis()

    init {
        programRepository.getProgramsForChannel(
            channelNumber = channel
        ).observe(lifecycleOwner, Observer<List<Program>> {
            loading = false
            programs = it ?: setOf()
            update()
        })

        recordingRepository.getRecordings(
        ).observe(lifecycleOwner, Observer<List<Recording>> {
            loading = false
            recordings = it ?: setOf()
            update()
        })
    }

    private fun update() {
        var items: Collection<ProgramOverviewViewState> = setOf()
        val timeSpanMillis = TimeUnit.HOURS.toMillis(6)
        val thresholdMoment = moment + timeSpanMillis

        for (program in programs) {
            if (program.startTime ?: 0 > thresholdMoment || program.startTime ?: 0< moment) {
                continue
            }

            val recording = recordings.find { it.programId == program.id }

            val item = ProgramOverviewViewState(
                id = program.id,
                startTime = program.startTime ?: 0,
                onClickListener = View.OnClickListener {

                    if (recording != null) {
                        loading = true
                        recordingRepository.cancelRecording(
                            recordingId = recording.recordingId
                        )
                        navigationService.navigateTo(1)
                    } else {
                        attemptProgramBooking(
                            program = program
                        )
                    }
                }
            )

            val startMillis = program.startTime ?: 0
            val durationMillis = TimeUnit.SECONDS.toMillis(program.duration?.toLong() ?: 0)
            val endMillis: Long = (program.startTime ?: 0) + durationMillis
            val formatter = DateFormat.getDateTimeInstance()
            val startDate = Date(startMillis)
            val startTimeText = formatter.format(startDate)
            val endDate = Date(endMillis)
            val endTimeText = formatter.format(endDate)
            val description = resources.getString(R.string.display_time_period, startTimeText, endTimeText)

            item.title = program.title
            item.iconImageUrl = baseUrl + program.poster
            item.statusAvailable = recording != null
            item.description = description

            items = items.plus(element = item)
        }

        viewState.adapter.setItems(items)
        viewState.loading = loading
    }

    private fun attemptProgramBooking(
        program: Program
    ) {
        var ranges = setOf<LongRange>()

        for (recording in recordings) {
            val recordingProgram = programs.find { it.id == recording.programId }
            val recordingProgramStartMillis = recordingProgram?.startTime ?: 0
            val recordingProgramDurationMillis = TimeUnit.SECONDS.toMillis(recordingProgram?.duration?.toLong() ?: 0)
            val recordingProgramEndMillis: Long = (recordingProgram?.startTime ?: 0) + recordingProgramDurationMillis
            val range = LongRange(recordingProgramStartMillis, recordingProgramEndMillis)
            ranges = ranges.plus(element = range)
        }

        val startMillis = program.startTime ?: 0
        val durationMillis = TimeUnit.SECONDS.toMillis(program.duration?.toLong() ?: 0)
        val endMillis: Long = (program.startTime ?: 0) + durationMillis

        val programRange = LongRange(startMillis, endMillis)
        var validBooking = true

        for (range in ranges) {
            val overlapping = Math.max(programRange.start, range.start) <= Math.min(programRange.endInclusive, programRange.endInclusive)

            if (overlapping) {
                validBooking = false
                break
            }
        }

        if (validBooking) {
            loading = true
            recordingRepository.bookRecording(
                programId = program.id,
                channelNumber = channel
            )
            navigationService.navigateTo(2)
        } else {
            navigationService.navigateTo(0)
        }
    }
}