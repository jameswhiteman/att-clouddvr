package com.att.clouddvr.network

import com.att.clouddvr.domain.Program
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ProgramWebservice {
    @GET(value = "programs")
    fun getProgramsForChannel(
        @Query(value = "channel_number") channelNumber: String
    ) : Call<List<Program>>
}