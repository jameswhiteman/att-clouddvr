package com.att.clouddvr.network

import com.att.clouddvr.domain.Recording
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface RecordingWebservice {
    @GET(value = "recording")
    fun getRecordings(): Call<List<Recording>>

    @POST(value = "recording")
    fun bookRecording(
        @Query(value = "program_id") programId: String,
        @Query(value = "channel_number") channelNumber: String
    ) : Call<JsonObject>

    @DELETE(value = "recording")
    fun cancelRecording(
        @Query(value = "recording_id") recordingId: String
    ) : Call<Any>
}
