package com.att.clouddvr.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import androidx.recyclerview.widget.SortedList
import androidx.recyclerview.widget.SortedListAdapterCallback
import com.att.clouddvr.R
import com.att.clouddvr.databinding.ProgramOverviewBinding
import com.att.clouddvr.viewstate.ProgramOverviewViewState

class ProgramOverviewAdapter : Adapter<ViewHolder>() {
    private val items: SortedList<ProgramOverviewViewState> = SortedList<ProgramOverviewViewState>(
        ProgramOverviewViewState::class.java,
        object : SortedListAdapterCallback<ProgramOverviewViewState>(this) {
            override fun areItemsTheSame(
                a: ProgramOverviewViewState?,
                b: ProgramOverviewViewState?
            ): Boolean {
                return a?.id == b?.id
            }

            override fun compare(
                a: ProgramOverviewViewState?,
                b: ProgramOverviewViewState?
            ): Int {
                return a?.startTime?.compareTo(b?.startTime ?: 0) ?: 0
            }

            override fun areContentsTheSame(
                a: ProgramOverviewViewState?,
                b: ProgramOverviewViewState?
            ): Boolean {
                return a?.statusAvailable == b?.statusAvailable &&
                        a?.description == b?.description &&
                        a?.iconImageUrl == b?.iconImageUrl &&
                        a?.title == b?.title
            }
        }
    )

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemView = inflater.inflate(R.layout.program_overview, parent, false)
        return object : ViewHolder(itemView) {}
    }

    override fun getItemCount(): Int {
        return items.size()
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {
        val binding = ProgramOverviewBinding.bind(holder.itemView)
        binding.viewState = items.get(position)
    }

    fun setItems(
        items: Collection<ProgramOverviewViewState>
    ) {
        this.items.replaceAll(items)
    }
}
