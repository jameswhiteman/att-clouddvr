package com.att.clouddvr.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import androidx.recyclerview.widget.SortedList
import androidx.recyclerview.widget.SortedListAdapterCallback
import com.att.clouddvr.R
import com.att.clouddvr.databinding.ChannelOverviewBinding
import com.att.clouddvr.viewstate.ChannelOverviewViewState

class ChannelOverviewAdapter : RecyclerView.Adapter<ViewHolder>() {
    private val items: SortedList<ChannelOverviewViewState> = SortedList<ChannelOverviewViewState>(
        ChannelOverviewViewState::class.java,
        object : SortedListAdapterCallback<ChannelOverviewViewState>(this) {
            override fun areItemsTheSame(
                a: ChannelOverviewViewState?,
                b: ChannelOverviewViewState?
            ): Boolean {
                return a?.id == b?.id
            }

            override fun compare(
                a: ChannelOverviewViewState?,
                b: ChannelOverviewViewState?
            ): Int {
                return a?.title?.compareTo(b?.title ?: "") ?: 0
            }

            override fun areContentsTheSame(
                a: ChannelOverviewViewState?,
                b: ChannelOverviewViewState?
            ): Boolean {
                return a?.title == b?.title
            }

        }
    )
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemView = inflater.inflate(R.layout.channel_overview, parent, false)
        return object : ViewHolder(itemView) {}
    }

    override fun getItemCount(): Int {
        return items.size()
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {
        val binding = ChannelOverviewBinding.bind(holder.itemView)
        binding.viewState = items.get(position)
    }

    fun setItems(
        items: Collection<ChannelOverviewViewState>
    ) {
        this.items.replaceAll(items)
    }
}