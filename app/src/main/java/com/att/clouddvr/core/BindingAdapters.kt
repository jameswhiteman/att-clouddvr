package com.att.clouddvr.core

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

@BindingAdapter("available")
fun setAvailable(view: View, available: Boolean) {
    val visibility = if (available) {
        View.VISIBLE
    } else {
        View.GONE
    }

    view.visibility = visibility
}

@BindingAdapter("onTouchListener")
fun setOnTouchListener(view: View, onTouchListener: View.OnTouchListener) {
    view.setOnTouchListener(onTouchListener)
}

@BindingAdapter("imageUrl")
fun setImageUrl(view: ImageView, imageUrl: String?) {
    Glide.with(view).load(imageUrl).diskCacheStrategy(DiskCacheStrategy.NONE).into(view)
}

@BindingAdapter("adapter")
fun setAdapter(view: RecyclerView, adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>) {
    view.adapter = adapter
}
