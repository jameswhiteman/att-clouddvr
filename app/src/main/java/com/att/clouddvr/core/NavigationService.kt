package com.att.clouddvr.core

interface NavigationService<T> {
    fun navigateTo(component: T)
}