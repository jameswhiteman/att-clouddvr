package com.att.clouddvr.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.att.clouddvr.R
import com.att.clouddvr.adapter.ProgramOverviewAdapter
import com.att.clouddvr.core.BASE_URL
import com.att.clouddvr.core.NavigationService
import com.att.clouddvr.databinding.ProgramsBinding
import com.att.clouddvr.factory.RepositoryFactory
import com.att.clouddvr.viewmodel.ProgramsViewModel
import com.att.clouddvr.viewstate.ProgramsViewState

class ProgramsActivity : AppCompatActivity() {
    private var binding: ProgramsBinding? = null
    private var viewModel: ProgramsViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.programs)

        supportActionBar?.let {
            it.setHomeButtonEnabled(true)
            it.setDisplayHomeAsUpEnabled(true)
        }

        val programOverviewAdapter = ProgramOverviewAdapter()
        val programsViewState = ProgramsViewState(
            adapter = programOverviewAdapter,
            onTouchListener = object : View.OnTouchListener {
                override fun onTouch(
                    view: View?,
                    event: MotionEvent?
                ): Boolean {
                    return true
                }
            }
        )
        binding?.viewState = programsViewState

        val repositoryFactory = RepositoryFactory()
        val channel = intent.getStringExtra("channel")

        viewModel = ProgramsViewModel(
            lifecycleOwner = this,
            viewState = programsViewState,
            channel = channel,
            baseUrl = BASE_URL,
            recordingRepository = repositoryFactory.createFakeRecordingRepository(),
            programRepository = repositoryFactory.createFakeProgramRepository(),
            resources = resources,
            navigationService = object : NavigationService<Int> {
                override fun navigateTo(component: Int) {
                    when (component) {
                        0 -> Toast.makeText(this@ProgramsActivity, R.string.display_cannot_book, Toast.LENGTH_SHORT).show()
                        1 -> Toast.makeText(this@ProgramsActivity, R.string.display_recording_cancel_success, Toast.LENGTH_SHORT).show()
                        2 -> Toast.makeText(this@ProgramsActivity, R.string.display_recording_book_success, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        )
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }

        return false
    }

    companion object {
        fun launchActivity(
            activity: Activity,
            channel: String
        ) {
            val bundle = Bundle()
            bundle.putString("channel", channel)
            val intent = Intent(activity, ProgramsActivity::class.java)
            intent.putExtras(bundle)
            activity.startActivity(intent)
        }
    }
}
