package com.att.clouddvr.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.att.clouddvr.R
import com.att.clouddvr.adapter.ChannelOverviewAdapter
import com.att.clouddvr.core.NavigationService
import com.att.clouddvr.databinding.ChannelsBinding
import com.att.clouddvr.viewmodel.ChannelsViewModel
import com.att.clouddvr.viewstate.ChannelsViewState

class ChannelsActivity : AppCompatActivity() {
    private var binding: ChannelsBinding? = null
    private var viewModel: ChannelsViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.channels)
        val channelOverviewAdapter = ChannelOverviewAdapter()
        val channelsViewState = ChannelsViewState(adapter = channelOverviewAdapter)
        binding?.viewState = channelsViewState

        viewModel = ChannelsViewModel(
            viewState = channelsViewState,
            navigationService = object : NavigationService<String> {
                override fun navigateTo(component: String) {
                    ProgramsActivity.launchActivity(
                        activity = this@ChannelsActivity,
                        channel = component
                    )
                }
            }
        )
    }
}
