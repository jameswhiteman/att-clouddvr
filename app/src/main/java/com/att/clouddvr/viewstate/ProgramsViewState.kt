package com.att.clouddvr.viewstate

import android.view.View
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.att.clouddvr.BR
import com.att.clouddvr.BR.loading
import com.att.clouddvr.adapter.ProgramOverviewAdapter

class ProgramsViewState(
    @Bindable val adapter: ProgramOverviewAdapter,
    @Bindable val onTouchListener: View.OnTouchListener
) : BaseObservable() {
    @Bindable var loading: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.loading)
        }
}
