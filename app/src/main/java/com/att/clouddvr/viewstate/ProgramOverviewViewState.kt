package com.att.clouddvr.viewstate

import android.view.View
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.att.clouddvr.BR

class ProgramOverviewViewState(
    val id: String,
    val startTime: Long,
    @Bindable val onClickListener: View.OnClickListener
) : BaseObservable() {
    @Bindable var title: String? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.title)
        }
    @Bindable var description: String? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.description)
        }

    @Bindable var iconImageUrl: String? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.iconImageUrl)
        }

    @Bindable var statusAvailable: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.statusAvailable)
        }
}
