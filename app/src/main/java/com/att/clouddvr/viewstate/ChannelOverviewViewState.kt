package com.att.clouddvr.viewstate

import android.view.View
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable

class ChannelOverviewViewState(
    val id: String,
    @Bindable var title: String?,
    @Bindable var onClickListener: View.OnClickListener
) : BaseObservable()
