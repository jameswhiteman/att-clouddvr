package com.att.clouddvr.viewstate

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.att.clouddvr.adapter.ChannelOverviewAdapter

class ChannelsViewState(
    @Bindable val adapter: ChannelOverviewAdapter
) : BaseObservable()