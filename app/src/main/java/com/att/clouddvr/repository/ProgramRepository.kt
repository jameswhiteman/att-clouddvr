package com.att.clouddvr.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.att.clouddvr.domain.Program
import com.att.clouddvr.network.ProgramWebservice
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProgramRepository(
    private val webservice: ProgramWebservice
) : Repository() {
    private var channelNumberToPrograms: Map<String, MutableLiveData<List<Program>>> = mapOf()

    fun getProgramsForChannel(
        channelNumber: String
    ): LiveData<List<Program>> {
        var data = channelNumberToPrograms[channelNumber]

        if (data == null) {
            data = MutableLiveData()
            data.value = listOf()
            channelNumberToPrograms = channelNumberToPrograms.plus(pair = channelNumber to data)
        }

        fetchProgramsForChannel(channelNumber = channelNumber)
        return data
    }

    private fun fetchProgramsForChannel(
        channelNumber: String
    ) {
        val data = channelNumberToPrograms[channelNumber]

        webservice.getProgramsForChannel(channelNumber = channelNumber).enqueue(object : Callback<List<Program>> {
            override fun onResponse(
                call: Call<List<Program>>,
                response: Response<List<Program>>
            ) {
                data?.postValue(response.body())
            }

            override fun onFailure(
                call: Call<List<Program>>,
                t: Throwable
            ) {
                data?.postValue(listOf())
            }
        })

    }
}