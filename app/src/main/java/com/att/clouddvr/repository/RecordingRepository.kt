package com.att.clouddvr.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.att.clouddvr.domain.Recording
import com.att.clouddvr.network.RecordingWebservice
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RecordingRepository(
    private val webservice: RecordingWebservice
) {
    private var recordingData: MutableLiveData<List<Recording>>? = null

    fun getRecordings(): LiveData<List<Recording>> {
        var data =  recordingData

        if (data == null) {
            data = MutableLiveData()
            recordingData = data
        }

        fetchRecordings()

        return data
    }

    fun bookRecording(
        programId: String,
        channelNumber: String
    ) {
        webservice.bookRecording(
            programId = programId,
            channelNumber = channelNumber
        ).enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                fetchRecordings()
            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                fetchRecordings()
            }
        })
    }

    fun cancelRecording(
        recordingId: String
    ) {
        webservice.cancelRecording(
            recordingId = recordingId
        ).enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                fetchRecordings()
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                fetchRecordings()
            }
        })
    }

    private fun fetchRecordings() {
        webservice.getRecordings().enqueue(object : Callback<List<Recording>> {
            override fun onResponse(call: Call<List<Recording>>, response: Response<List<Recording>>) {
                recordingData?.postValue(response.body())
            }

            override fun onFailure(call: Call<List<Recording>>, t: Throwable) {
                recordingData?.postValue(listOf())
            }
        })
    }
}
