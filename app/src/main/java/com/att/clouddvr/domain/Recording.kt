package com.att.clouddvr.domain

import com.google.gson.annotations.SerializedName

data class Recording(
    @SerializedName(value = "recording_id") val recordingId: String,
    @SerializedName(value = "program_id") val programId: String,
    @SerializedName(value = "channel_id") val channelId: String
)
