package com.att.clouddvr.domain

import com.google.gson.annotations.SerializedName

data class Program(
    @SerializedName(value = "id") val id: String,
    @SerializedName(value = "title") val title: String?,
    @SerializedName(value = "startTime") val startTime: Long?,
    @SerializedName(value = "duration") val duration: Int?,
    @SerializedName(value = "poster") val poster: String
)
