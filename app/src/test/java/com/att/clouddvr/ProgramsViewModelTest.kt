package com.att.clouddvr

import android.content.res.Resources
import android.view.MotionEvent
import android.view.View
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.att.clouddvr.adapter.ProgramOverviewAdapter
import com.att.clouddvr.core.NavigationService
import com.att.clouddvr.domain.Program
import com.att.clouddvr.domain.Recording
import com.att.clouddvr.repository.ProgramRepository
import com.att.clouddvr.repository.RecordingRepository
import com.att.clouddvr.viewmodel.ProgramsViewModel
import com.att.clouddvr.viewstate.ProgramsViewState
import io.mockk.every
import io.mockk.mockk
import org.junit.Before
import org.junit.Test

class ProgramsViewModelTest {
    private val lifecycleOwner = mockk<LifecycleOwner>()
    private val baseUrl = "example"
    private val programRepository = mockk<ProgramRepository>()
    private val recordingRepository = mockk<RecordingRepository>()
    private val programOverviewAdapter = ProgramOverviewAdapter()
    private val viewState = ProgramsViewState(
        adapter = programOverviewAdapter,
        onTouchListener = object : View.OnTouchListener {
            override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
                return true
            }
        }
    )
    private val resources = mockk<Resources>()
    private val navigationService = mockk<NavigationService<Int>>()
    private val channelNumber = "1"
    private val programData = mockk<MutableLiveData<List<Program>>>()
    private val recordingData = mockk<MutableLiveData<List<Recording>>>()

    @Before
    fun prepare() {
        every {programData.observe(lifecycleOwner, any())} answers {}
        every {recordingData.observe(lifecycleOwner, any())} answers {}
        every { programRepository.getProgramsForChannel(channelNumber) } answers { programData }
        every { recordingRepository.getRecordings() } answers { recordingData }
    }

    @Test
    fun init_default_success() {
        createViewModel()
        assert(!viewState.loading)
        assert(viewState.adapter.itemCount == 0)
    }

    private fun createViewModel(): ProgramsViewModel {
        return ProgramsViewModel(
            lifecycleOwner = lifecycleOwner,
            programRepository = programRepository,
            channel = channelNumber,
            baseUrl = baseUrl,
            viewState = viewState,
            recordingRepository = recordingRepository,
            resources = resources,
            navigationService = navigationService
        )
    }
}
